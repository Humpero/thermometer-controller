package com.thermocontroll.noahg.termometercontroller;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.content.pm.PackageManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    private Camera camera;
    private boolean isFlashOn;
    private boolean hasFlash;
    private Parameters params;

    private Button start_button;
    private Button start_2_button;
    private Button start_img_button;
    public GifImageView image_controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckIfHasFlash();
        getCamera();

        start_button = (Button) findViewById(R.id.start_Button);
        start_2_button = (Button) findViewById(R.id.start_2_Button);
        start_img_button = (Button) findViewById(R.id.start_img_Button);
        image_controller = (GifImageView) findViewById(R.id.main_image_Controller);

        start_button.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        int HzToRunAt = Integer.parseInt(((EditText)findViewById(R.id.Number_Hz)).getText().toString()),
                                startingNumber = Integer.parseInt(((EditText)findViewById(R.id.Starting_Number)).getText().toString()),
                                number1 = Integer.parseInt(((EditText)findViewById(R.id.Number_1)).getText().toString()),
                                number2 = Integer.parseInt(((EditText)findViewById(R.id.Number_2)).getText().toString()),
                                number3 = Integer.parseInt(((EditText)findViewById(R.id.Number_3)).getText().toString());

                        String binaryLine = (String.format("%8s", Integer.toBinaryString(startingNumber)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number1)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number2)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number3)).replace(' ', '0'));

                        Log.e("BINARY LINE: ", binaryLine);

                        for (char i : binaryLine.toCharArray())
                        {
                            //long startTime = System.currentTimeMillis();

                            if (i == '1')
                            {
                                turnOnFlash();
                            }
                            else
                            {
                                turnOffFlash();
                            }

                            long sleepFor = (1000 / HzToRunAt)/* - (System.currentTimeMillis() - startTime)*/;

                            try {
                                Thread.sleep(sleepFor);                 //1000 milliseconds is one second.
                            } catch(InterruptedException ex) {
                                Thread.currentThread().interrupt();
                            }
                        }

                        turnOffFlash();
                    }
                }
        );

        start_2_button.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        int HzToRunAt = Integer.parseInt(((EditText)findViewById(R.id.Number_Hz)).getText().toString()),
                                startingNumber = Integer.parseInt(((EditText)findViewById(R.id.Starting_Number)).getText().toString()),
                                number1 = Integer.parseInt(((EditText)findViewById(R.id.Number_1)).getText().toString()),
                                number2 = Integer.parseInt(((EditText)findViewById(R.id.Number_2)).getText().toString()),
                                number3 = Integer.parseInt(((EditText)findViewById(R.id.Number_3)).getText().toString());

                        String binaryLine = (String.format("%8s", Integer.toBinaryString(startingNumber)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number1)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number2)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number3)).replace(' ', '0'));

                        Log.e("BINARY LINE: ", binaryLine);

                        for (char i : binaryLine.toCharArray())
                        {
                            long startTime = System.currentTimeMillis();

                            if (i == '1')
                            {
                                turnOnFlash();
                            }
                            else
                            {
                                turnOffFlash();
                            }

                            long sleepFor = (1000 / HzToRunAt) - (System.currentTimeMillis() - startTime);

                            if(sleepFor < 0)
                            {
                                RateToHigh();
                                break;
                            }

                            try {
                                Thread.sleep(sleepFor);                 //1000 milliseconds is one second.
                            } catch(InterruptedException ex) {
                                Thread.currentThread().interrupt();
                            }
                        }

                        turnOffFlash();
                    }
                }
        );

        start_img_button.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        int HzToRunAt = Integer.parseInt(((EditText)findViewById(R.id.Number_Hz)).getText().toString()),
                                startingNumber = Integer.parseInt(((EditText)findViewById(R.id.Starting_Number)).getText().toString()),
                                number1 = Integer.parseInt(((EditText)findViewById(R.id.Number_1)).getText().toString()),
                                number2 = Integer.parseInt(((EditText)findViewById(R.id.Number_2)).getText().toString()),
                                number3 = Integer.parseInt(((EditText)findViewById(R.id.Number_3)).getText().toString());

                        String binaryLine = (String.format("%8s", Integer.toBinaryString(startingNumber)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number1)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number2)).replace(' ', '0')) +
                                (String.format("%8s", Integer.toBinaryString(number3)).replace(' ', '0'));

                        Log.e("BINARY LINE: ", binaryLine);

                        ByteArrayOutputStream gif = new ByteArrayOutputStream();
                        AnimatedGifEncoder encoder = new AnimatedGifEncoder();
                        encoder.setDelay(1000/HzToRunAt);
                        encoder.setRepeat(-1);
                        encoder.start(gif);

                        for (char i : binaryLine.toCharArray()) {
                            if (i == '1')
                            {
                                encoder.addFrame(BitmapFactory.decodeResource(getResources(), R.drawable.white));
                            }
                            else
                            {
                                encoder.addFrame(BitmapFactory.decodeResource(getResources(), R.drawable.black));
                            }
                        }

                        encoder.finish();

                        try {
                            GifDrawable gifImage = new GifDrawable(gif.toByteArray());
                            image_controller.setImageDrawable(gifImage);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    private void CheckIfHasFlash()
    {
        hasFlash = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!hasFlash) {
            // device doesn't support flash
            // Show alert message and close the application
            AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                    .create();
            alert.setTitle("Error");
            alert.setMessage("Sorry, your device doesn't support using the flash light!");
            alert.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // closing the application
                    finish();
                }
            });
            alert.show();
        }
    }

    private void RateToHigh()
    {
        AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                .create();
        alert.setTitle("Error");
        alert.setMessage("Sorry, the refresh rate you set is too high!");
        alert.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // closing the application
                //finish();
            }
        });
        alert.show();
    }

    private void getCamera() {
        if (camera == null) {
            try {
                camera = Camera.open();
                params = camera.getParameters();
            } catch (RuntimeException e) {
                //Log.e("Camera Error. Failed to Open. Error: ", e.getMessage());
            }
        }
    }

    private void turnOnFlash() {
        if (!isFlashOn) {
            if (camera == null || params == null) {
                return;
            }

            params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();
            isFlashOn = true;
        }

    }

    private void turnOffFlash() {
        if (isFlashOn) {
            if (camera == null || params == null) {
                return;
            }

            params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            isFlashOn = false;
        }
    }

}

